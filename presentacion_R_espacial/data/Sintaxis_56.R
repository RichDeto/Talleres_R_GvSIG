#########################################################
################ TALLER R ESPACIAL ######################
#########################################################

# rm(list = ls()); setwd("X:/CURSOS (General)/Taller R espacial/Materiales"); load("ejercicio.Rdata")
# x <- c("sp","devtools","plyr","shapefiles","rgdal","dplyr","ggmap","RColorBrewer","maptools","rgeos",
#        "deldir","geometry","splancs","cclust","classInt","leaflet","shiny")
# lapply(x, library, character.only = TRUE)

############### 5) GRÁFICOS ESTÁTICOS #######################

# Definir algunos parámetros para AT
plotvar <- round(AT@data$p_icc*100,2)
nclr <- 5
plotclr <- brewer.pal(nclr,"BuPu")
class <- classIntervals(plotvar, nclr, style = "quantile")
colcode <- findColours(class, plotclr)

# Generar la salida gráfica para AT
pdf("AT1.pdf") # la función jpeg() da por su parte una imagen
plot(municipios, col = "transparent")
plot(AT, col = colcode, add = T)
plot(socats, col = "green", pch = 16, cex = 0.5, add = T)
title(main = "Áreas atención SOCAT en Montevideo",sub = "Escenario 1")
legend("bottomright", legend = names(attr(colcode, "table")), 
       fill = attr(colcode, "palette"), cex = 0.6, bty = "n")
dev.off()

# Definir algunos parámetros para AT2
plotvar2 <- round(AT2@data$p_icc*100,2)
nclr2 <- 5
plotclr2 <- brewer.pal(nclr2,"BuPu")
class2 <- classIntervals(plotvar2, nclr2, style = "quantile")
colcode2 <- findColours(class2, plotclr2)

# Generar la salida gráfica para AT2
pdf("AT2.pdf")
plot(municipios, col = "transparent")
plot(AT2, col = colcode2, add = T)
title(main = "Áreas atención SOCAT en Montevideo",sub = "Escenario 2")
legend("bottomright", legend = names(attr(colcode2, "table")), 
       fill = attr(colcode2, "palette"), cex = 0.6, bty = "n")
dev.off()

################## 6) mapa web ############################
AT_latlong <- spTransform(AT,CRS("+init=epsg:4326"))
AT2_latlong <- spTransform(AT2,CRS("+init=epsg:4326"))

qpal <- colorQuantile("BuPu", round(AT@data$p_icc*100,2), n = 5)

# o para instalarlo desde Github, correr:
# devtools::install_github("rstudio/leaflet") 
# library(leaflet)
map <- leaflet() %>%
        # para setear el mosaico de fondo
        addTiles() %>%
        
        # agregamos las capas 
        addPolygons(data = AT_latlong, weight = 2, fillColor = colcode, 
                    stroke = FALSE, fillOpacity = 0.5, smoothFactor = 0.5
                    , popup = AT_latlong@data$areaterr) %>%
        addPolygons(data = AT2_latlong, weight = 2, fillColor = colcode,
                    stroke = FALSE, fillOpacity = 0.5, smoothFactor = 0.5) %>%
        
        # agregamos la leyenda
        addLegend("bottomright",title = "AT por quintiles de ICC",pal = qpal, 
                  values = round(AT2@data$p_icc*100,2), opacity = 1)

# imprimimos el mapa
map  

#########################################################################

# Transformemos ahora el sistema de coordenadas de los socats
socats_latlong <- spTransform(socats,CRS("+init=epsg:4326"))

# Generemos una etiqueta para las AT
etiqueta <- paste(sep = "<br/>",
                  paste0("<b>",as.character(AT_latlong@data$areaterr),"</b>"),
                  as.character(round(AT@data$p_icc*100,2)))

# Generemos una etiqueta para las AT2
etiqueta2 <- paste(sep = "<br/>",
                   paste0("<b>",as.character(AT2_latlong@data$areaterr),"</b>"),
                   as.character(round(AT2@data$p_icc*100,2)))

map <- leaflet() %>%
        # Ahora generamos un groupo de base 
        addTiles(group = "OSM (default)") %>%
        addProviderTiles("Esri.WorldImagery", group = "Satelital") %>%
        
        # Generamos el grupo de capas 
        addPolygons(data = AT_latlong, weight = 2, fillColor = colcode, 
                    stroke = FALSE, fillOpacity = 0.5, smoothFactor = 0.5,
                    popup = etiqueta,  group = "AT") %>%
        addPolygons(data = AT2_latlong, weight = 2, fillColor = colcode,
                    stroke = FALSE, fillOpacity = 0.5, smoothFactor = 0.5,
                    popup = etiqueta, group = "AT2") %>%
        addCircleMarkers(data = socats_latlong, radius = 3, color = "black",
                         stroke = FALSE, fillOpacity = 0.5, group = "socats",
                         popup = socats_latlong@data$rh) %>%
        
        # Agregamos controles para las capas
        addLayersControl(baseGroups = c("OSM (default)", "Satelital"),
                         overlayGroups = c("AT", "AT2","socats"),
                         options = layersControlOptions(collapsed = FALSE)) %>%
        
        # Agragamos la leyenda
        addLegend("bottomright",title = "Quintiles de ICC",pal = qpal, 
                  values = round(AT2@data$p_icc*100,2), opacity = 1) 

# imprime el mapa
map  

# Publicamos el resultado
# library(shiny)
# library(leaflet)


ui <- fluidPage(  titlePanel("Ejemplo taller"),
                  sidebarLayout(
                    sidebarPanel(
                      h3("Aplicación shiny"),
                      p("Este visualizador web es un ejemplo sencillo de cómo utilizar este paquete junto a leaflet."),
                      p("Se incluye un panel a la izquierda usando un poco de código HTML"),
                      p("Esta app corre localmente a menos que se aloje en un servidor"),
                      br(),
                      br(),
                      br(),
                      br(),
                      br(),
                      br(),
                      span("App con Shiny", style = "color:blue")
                    ),
                    mainPanel(
                      leafletOutput("mymap")
                    )))

server <- function(input, output, session) {
  
  output$mymap <- renderLeaflet({
    map <- leaflet() %>%
      # Ahora generamos un groupo de base 
      addTiles(group = "OSM (default)") %>%
      addProviderTiles("Esri.WorldImagery", group = "Satelital") %>%
      
      # Generamos el grupo de capas 
      addPolygons(data = AT_latlong, weight = 2, fillColor = colcode, 
                  stroke = FALSE, fillOpacity = 0.5, smoothFactor = 0.5,
                  popup = etiqueta,  group = "AT") %>%
      addPolygons(data = AT2_latlong, weight = 2, fillColor = colcode,
                  stroke = FALSE, fillOpacity = 0.5, smoothFactor = 0.5,
                  popup = etiqueta, group = "AT2") %>%
      addCircleMarkers(data = socats_latlong, radius = 3, color = "black",
                       stroke = FALSE, fillOpacity = 0.5, group = "socats",
                       popup = socats_latlong@data$rh) %>%
      
      # Agregamos controles para las capas
      addLayersControl(baseGroups = c("OSM (default)", "Satelital"),
                       overlayGroups = c("AT", "AT2","socats"),
                       options = layersControlOptions(collapsed = FALSE)) %>%
      
      # Agragamos la leyenda
      addLegend("bottomright",title = "Quintiles de ICC",pal = qpal, 
                values = round(AT2@data$p_icc*100,2), opacity = 1) 
  })
}

shinyApp(ui, server)
