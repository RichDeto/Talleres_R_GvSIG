#########################################################
################ TALLER R ESPACIAL ######################
#########################################################

######### 0) PRIMEROS PASOS EN R #############

# Limpiar el entorno # 
rm(list = ls()) #ls() lista objetos en memoria 

## Clases de objetos en R ##

#creamos un vector numérico
vector_num <- c(1,0,1,1,1,0)
#vector_num=rep(12,5);vector_num=seq(1,5,0.5)
class(vector_num)

#creamos un vector caracter
vector_car <- c("1","0","1","1","1","0")
#vector_car=as.character(vector_num)
class(vector_car)

#creamos un vector lógico
vector_log <- vector_num > 0
#vector_log=c(T,F,T,T)
class(vector_log)

#creamos un vector factor, asignando etiquetas a los valores
vector_fac <- factor(c("1","0","1","1","1","0"),
										 levels = unique(c("1","0","1","1","1","0")),
										 labels = c("S�","No"))
class(vector_fac)

#creamos una matriz de 2 x 2
matriz <- matrix(c(1,0,8,14,5,6), nrow = 3, ncol = 2)
#matriz <- matrix(vector_num,nrow=3)
class(matriz)
colnames(matriz) <- c("V1","V2")

#creamos un data.frame a partir de la matriz 
base <- data.frame(matriz)
class(base)
names(base) #colnames()
dim(base)

base <- cbind(base,vector_fac)

#creamos una lista a partir del data.frame y el vector caracter
lista <- list(base,vector_car)
class(lista)

#accedemos a los elementos de la lista
lista[[1]] #el data.frame
lista[[2]] #el character

lista[[1]]$V1 #la primera variable del data.frame
names(lista[[1]])

#--------VOLVEMOS A LA PRESENTACIÓN #-----------#