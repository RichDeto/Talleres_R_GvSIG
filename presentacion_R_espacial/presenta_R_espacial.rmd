---
title: Taller R espacial
subtitle: V Jornadas uruguayas de gvSIG y III de tecnologías libres de información geográfica y datos abiertos - Uruguay
header-includes:
- \usepackage[utf8]{inputenc}
- \usepackage[spanish]{babel}
- \usepackage{latexsym} 
- \usepackage{amsmath}
- \usepackage{amssymb}
- \usepackage{float} 
- \usepackage{graphicx}
- \usepackage{keystroke}
- \usepackage{graphics}
- \usepackage{colortbl}
- \usepackage{color}
- \usepackage{url}
- \usepackage{multicol}
- \hypersetup{colorlinks=true, linkcolor=blue, filecolor=magenta, urlcolor=cyan,  }
- \usepackage{caption}
- \captionsetup[figure]{labelformat=empty}      
output: 
        beamer_presentation: 
                theme: "metropolis"
                colortheme: "dolphin"
                fonttheme: "structurebold"
---

\begin{figure}[h]	
\begin{minipage}[b]{9cm}
\hfill 	
\vfill
\vspace{-1cm}
\begin{center}	
\includegraphics[scale=0.4]{imagenes/wordcloud}\\
\scriptsize
\includegraphics[scale=0.5]{imagenes/gm}\hspace{0.1cm} Gabriela Mathieu\\
\textcolor{gray}{\textit{gmathieu@mides.gub.uy}}\\ 
\includegraphics[scale=0.5]{imagenes/rd} \hspace{0.1cm} Richard Detomasi\\
\textcolor{gray}{\textit{rdetomasi@mides.gub.uy}}\\ 
\vspace{1cm}
\textcolor{white}{aaaaa}
Este trabajo está bajo licencia Creative Commons BY-NC-SA 3.0 \\        
\includegraphics[scale=0.3]{imagenes/by-nc-sa}
\end{center}
\end{minipage}
\end{figure}

### SIG o GDS?

Diferencias principales entre el campo de los Sistemas de Información Geográficos (**SIG**) y el de la _Geographic Data Science_ (**GDS**):

| Atributos      | SIG       | GDS    |
|----------------|---------------|----------------|
| Disciplinas   | Geografía  | Geografía, Computación, Estadística |
| Foco | Interfaz Gráfica  | Código |
| Reproducible  | Mínimo   | Máximo |

Esta distinción es importante por ejemplo en el contexto de tener un impacto en las políticas: al ser abierto y transparente (y por lo tanto usar código en lugar de una interfaz gráfica de usuario no reproducible) la investigación maximiza sus posibilidades de alterar las decisiones.

### **sp** y **sf**

> - **sf** y **sp** son las librerías más importantes de R para manejar información vectorial; **sf** sería el sucesor de **sp**, pero este último sigue siendo ampliamente usado. Sobre todo, si consideramos que muchas otras librerias de R aun dependen de las funciones y clases de **sp**.
> - **raster** es una extensión de las clases de datos espaciales para trabajar con rásters.
> - Hay muchas maneras de visualizar información espacial en R, a partir de librerías tales como **ggplot2**, **rasterVis**, **tmap**, **leaflet**, o **mapview**.
> - Además es muy sencillo conectar R con otros softwares de SIG - GRASS GIS (**rgrass7**), SAGA (**RSAGA**), QGIS (**RQGIS** y **qgisremote**), incluso ArcGIS (**arcgisbinding**)
> - La mayoría de las funciones en este paquete empiezan en "st_".

### **sf**

> - **sf** es un paquete desarrollado recientemente para trabajar con indormación espacial (vectorial)    
> - Combina las funcionalidades de 3 librerías previas: **sp**, **rgeos** y **rgdal**     
> - Tiene varias ventajas, incluyendo:    

> - Mayor velocidad para importar y exportar los datos    
> - Más tipos de geometrías soportadas
> - Compatibilidad con **tidyverse**

### **tidyverse**

> - En R, paquetes como **ggplot2** y **dplyr** han adquirido gran popularidad, y ahora forman parte de **tidyverse**
> - Estos paquete se basan en los principios de 'tidy data' para mayor consistencía y velocidad de procesamiento:

> - Cada Variable forma una columna.    
> - Cada observación forma una fila.     
> - Cada tipo de unidad observacional forma una tabla.    

> - Históricamente los paquetes espaciales de R (**sp** en especial) no son compatibles con **tidyverse**

### **sf** vs **sp**

> - La librería **sp** es la predecesora de **sf** 
> - Junto con los paquetes **rgdal** y **rgeos**, it creates a powerful tool to works with spatial data
> - Muchos paquetes espaciales de R todavía dependen del paquete **sp**, por lo tanto, es importante saber cómo convertir objetos **sp**  a  **sf** y viceversa
> - La estructura en los objetos **sp** es más complicada - `str(world_sf)` vs `str(world_sp)`
> - Además, muchas de las funciones de **sp** no son "pipeables" (o sea no combinable con **tidyverse**)

### de **sf** a **sp** y viceversa..

```{r eval=FALSE, message=FALSE, warning=FALSE, paged.print=FALSE}
library(sp); library(sf)
world <- st_read("data/wrld.shp")
# Para transformar de SF a SP
world_sp <- as(world, "Spatial")

# Para transformar de SP a SF
world_sf <- st_as_sf(world_sp)
```

### Manipulación de atributos vectoriales 

> - La información geográfica vectorial es bien manejada por `sf`, en una clase que extiende las capacidades de la clase `data.frame`.
> - Estos objetos `sf` tienen una columna por variable (por ejemplo 'nombre') y una fila por observación, o *elemento* (por ejemplo cada parada de bus).
> - Los objetos `sf` también tienen una columna especial que contiene los datos de geometría, usualmente lla amdo `geom` o `geometry`.
> - La columna `geometry` es especial ya que es una *list-colum*, con lo que puede contener multiples elementos geográficos (puntos, líneas, polígonos) por fila.

### Manipulación de atributos vectoriales 

 - Ya vimos algunos *métodos genéricos* como ser `plot()` o `summary()` en objetos `sf`.
 - **sf** además permite usar métodos propios de los `Data.Frames` en objetos `sf`:

```{r, eval=FALSE}
methods(class = "sf") 
```

o    

```{r, eval=FALSE, echo=FALSE}
# otra forma de ver los métodos posibles para sf:
attributes(methods(class = "sf"))$info %>% 
  filter(!visible)
```

### Manipulación de atributos vectoriales 

Aquí los primeros 12 métodos para objetos sf:

```{r}
#>  [1] aggregate      cbind        coerce               
#>  [4] initialize     merge        plot                 
#>  [7] print          rbind        [                    
#> [10] [[<-           $<-          show                 
```

### Manipulación de atributos vectoriales

> - Muchas de estas funciones, incluyendo `rbind()` (para pegar filas de objetos juntas) o `$<-` (para crear nuevas columnas) fueron desarrolladas para data frames.
> - Los objetos `sf` soportan las clases `tibble` y `tbl` usadas en `tidyverse`
> - En definitiva, **sf** permite que toda la potencia de las capacidades de análisis de datos de R se desate en datos geográficos.
> - Manos a la obra...

### Manipulación de atributos vectoriales

Empecemos por usar funciones del _base_ en  base la base `world`:

```{r message=FALSE, warning=FALSE, include=FALSE, paged.print=FALSE}
library(sf)
world = spData::world
```

```{r }
# es un objeto de 2 dimensiones, con filas y columnas
dim(world) 

# cuántas filas?
nrow(world) 

# cuántas columnas?
ncol(world) 
```

### Manipulación de atributos vectoriales

Esta base tiene 10 columnas no-geográficas (y una `list-column` de geometrías) con casi 200 filas representates de los paises del mundo.

Extraer los atributos de un objeto `sf`, es lo mismo que eliminarle sus geometrías:

```{r}
world_df = st_set_geometry(world, NULL)
class(world_df)
```

### Selección por atributos

> - _Base R_ incluye las funciones de selección `[`, `subset()` y `$`.
> - y **dplyr** aporta `select()`, `slice()`, `filter()`, y `pull()`.
> - ambos grupos de funciones conservan los componentes espaciales de los objetos `sf`.

### Selección por atributos

> - `select()` selecciona columnas por nombre o por posición.
> - `pull()`extrae una columna como un vector
> - `slice()` es el equivalente de `select()` pero para las filas.
> - `filter()` es el equivalente en **dplyr** al `subset()` del _base_ de R.
> - Otras funciones: `contains()`, `starts_with()` y `num_range()` 

### Conectando funciones

> - Un beneficio de **dplyr** es su compatibilidad con el operador ` %% `.
> - Este 'pipe' de R, toma su nombre del pipe de Unix  `|`, y es parte del paquete **magrittr**
> - Su función es "conectar" la salida de un comando anterior al primer argumento de la siguiente función.
> - Esto permite encadenar comandos de análisis de datos, pasando el marco de datos de una función a la siguiente.
> - Una ventajas adicional es que fomenta la adición de comentarios a funciones autónomas y permiten líneas simples comentadas sin romper el código.

### Agregación de atributos

> - Las operaciones de agregación resumen los conjuntos de datos por una 'variable de agrupación' (típicamente una columna de atributo) o un objeto espacial.
> - Para calcular el número de personas por continente en base al objeto `world`
> - usaremos la columna` pop` que contiene la población por país y la variable de agrupación `continent`.
> - En la base R esto se hace con `aggregate()` y `sum()` de la siguiente manera:

### Agregación de atributos

```{r, eval=FALSE}
aggregate(pop ~ continent, 
          FUN = sum, 
          data = world, 
          na.rm = TRUE)
```

### Agregación de atributos

- `summarize()` es el equivalente en **dplyr** de `aggregate()`,
- y usa la función `group_by()` para agrupar la variable. 
- Se implementaría así:

```{r, eval=FALSE}
group_by(world, continent) %>%
  summarize(pop = sum(pop, na.rm = TRUE))
```

### Agregación de atributos

> - Este enfoque es flexible, lo que permite nombrar las columnas resultantes.
> - El omitir la variable de agrupación pone todo en un grupo.
> - Esto significa que `summarize ()` se puede usar para calcular la población total de la Tierra (~ 7 mil millones) y el número de países. 
> - Utilicemos `sum()` y `n()` para generar las columnas `pop` y `n_countries` 

### Agregación de atributos

```{r, eval=FALSE}
world %>% 
  summarize(pop = sum(pop, na.rm = TRUE),
            n_countries = n())
```

### Agregación de atributos

Combinemos todo lo anterior para identificar los 3 continentes más poblados (usando `dplyr::n()` ) y el número de países que contienen:

```{r eval=FALSE, message=FALSE, warning=FALSE, paged.print=FALSE}
world %>% 
  dplyr::select(pop, continente = continent) %>% 
  group_by(continente) %>% 
  summarize(población = sum(pop, na.rm = TRUE), n_paises = n()) %>% 
  top_n(n = 3, wt = población) %>%
  st_set_geometry(value = NULL)
```

### Agregación de atributos

```{r continents, echo=FALSE, message=FALSE, warning=FALSE}
world %>% 
  dplyr::select(pop, continente = continent) %>% 
  dplyr::group_by(continente) %>% 
  dplyr::summarize(población = sum(pop, na.rm = TRUE), n_paises = n()) %>% 
  dplyr::top_n(n = 3, wt = población) %>%
  st_set_geometry(value = NULL) %>% 
  knitr::kable(caption = "Los 3  continentes más poblados, y su número de países.")
```

### Combinando objetos

> - `Join` combina tablas basadas en una variable compartida ("key")
> - **dplyr** presenta varias funciones para ello: 
>  - `left_join()` - Une las filas coincidentes de b en a
>  - `right_join()` - Une las filas coincidentes de a en b
>  - `inner_join()` - Une reteniendo solo las filas de ambos conjuntos, 
>  - `full_join` - Une los datos conservando todos los valores, todas las filas
>  - `semi_join()` - Todas las filas en a que tienen una coincidencia en b 
>  - `anti_join()` - Todas las filas en a que no tienen una coincidencia en b
> - Estos nombres de funciones siguen las convenciones utilizadas en el lenguaje de bases de datos [SQL](http://r4ds.had.co.nz/relational-data.html).

### Combinando objetos

\includegraphics[scale=0.3]{imagenes/join}

### Combinando objetos

> - Las funciones "..._join" de **dplyr** trabaja tanto con data.frames como con objetos `sf`, the only important difference being the `geometry` list column.
> - El orden de los factores altera el resultado... la clase del primer objeto es la que conserva el resultado.
> - Nos centraremos en los left e inner "..._join" que son los más utilizados, que utilizan la misma sintaxis que los otros tipos de unión.
> - Pasemos al ejercicio...

### Combinando objetos

| dplyr            | base                     |
|------------------|--------------------------|
| left_join(x, y)  | merge(x, y, all.x=TRUE)  |
| right_join(x, y) | merge(x, y, all.y=TRUE)  |

### Join espacial

> - Para la unión basada en intersecciones espaciales (de cualquier tipo), se usa st_join
> - El método de join utilizado es siempre left join, manteniendo los registros del primer atributo
> - del mismo modo la geometría también será del primer argumento

### Join espacial

Por defecto usa la función intersect()

```{r, eval=FALSE}
st_join(x, y) 
```

Pero se pueden utilizar otras funciones:

```{r, eval=FALSE}
st_join(x, y, join = st_covers)
```
